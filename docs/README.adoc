= role_unit

:toc: left
:toclevels: 3

== more than basic usage

=== about namespace

Role unit functions are prefixed by ru_. Role_unit environment variables for configuration are prefixed by RU_. Internal variables are prefixed with ru_. We keep it that way to minimize impact on tested environment.

=== parameters

No environment variable is mandatory. It is however better to set at least RU_ENV_IMAGE. It defines the linux flavor you will run your tests against. Default configuration pulls docker images from role_unit containers repository (registry.gitlab.com/role_unit/role_unit_containers). Actually, you can choose among :

* debian 12 (bookworm)
* debian 11 (bullseye)
* debian 10 (buster)
* debian 9 (stretch)
* ubuntu 22.04
* ubuntu 20.04
* ubuntu 18.04
* centos 8
* archlinux

This list is completed by some additional containers. For instance the '_cached' containers where package manager has a cache buitin to install package whithout extra steps (cache is being cleaned in standard container to reduce image size). Full list of those containers can be found in the https://gitlab.com/role_unit/role_unit_containers/container_registry[container registry of role_unit_containers repository].

Role_unit behaviour can be changed using environment variables:

* RU_ENV_NAME defines the testing environment name. It is the name of the role you will test.
* RU_BACKEND defines the the backend you are using. Can be docker, podman, podman-compose or nocloud, defaults to docker.
* RU_ENV_DOCKER_REPO defines the docker repository to pull images from.
* RU_ENV_IMAGE defines the system image used to create test environment. Defaults to the latest Debian version available.
* RU_COUNT sets the number of containers or virtual machines
* RU_DEBUG when set to 1, will make role_unit not to stop containers after the run, so you can enter them to check things.
* RU_NOTMPFS defines wheter /tmp and /run are mounted as tmpfs. As a default they are, but there may be cases where, for instance, you want /tmp not to be mounted /noexec.

Following variable, only available with docker backend:

* RU_FIXED_NAMES if set to 1, will have containers names based on RU_ENV_NAME instead of UUIDs. This will break the hability to run tests in a conccurent way, but is needed when container names must be predictable. THIS VARIABLE IS TO BE DEPRECATED (there are now enough ways to get machines names and conccurency should not be compromised).

other role_unit variables may be used, but only to read values. Overwriting them may produce unexpected behaviours:

* ru_inventory is an absolute path to the inventory that will be used by ru_run_frontend
* ru_playbook is an absolute path to the playbook that will be used by ru_run_frontend
* ru_dir is the temporary working directory for the tests. You will for instance find the group_vars in it.

For example of the usage you can made of these variables, have a look at the tests_tuto file in your tests directory or the complex example at the end of this documentation.

=== available functions

Here is a list of functions available in the test file as a complement of bash_unit to manipulate the testing environment. For some examples see the tests_tuto file.

==== ru_init

Initiates testing environment. It sets the backend (creates containers or virtual machines depending on configuration) as well as the frontend (inventory with created containers/vms, playbook...).

==== ru_run_frontend

Actually applicates automation (as this is only frontend for now, runs ansible-playbook) on your testing environment.

==== ru_run [ -d ] [ -n N ] cmd ...

Runs the specified command on all of the servers or just on the one specified. N is the number of the server you want to run the command on (servers are numbered starting with 0).

If you specify -d option, this will launch command in a detached mode. There may be evolution in the future about that but for now those options must be sepcified in this order only.

==== ru_run_with_timeout -t T [ -n N ] cmd

Similar to ru_run but will retry cmd every second until timeout T is expired or command returns a 0 exit code (whatever comes first). Return code will be the exit code of the last run of the command.

This comes handy for instance if you start a service on the server and you want to wait until the service is effectively operational.

==== ru_fact [ -n N ] fact_name...

Returns the specified ansible fact for all of the servers or just on the one specified. N is the number of the server you want to get the fact from.

==== ru_fact_filter [ -n N ] fact_filter

Filters facts with the given expression and return the matching part of the facts in json format.

For instance, running ```ru_fact_filter anisble_default_ipv4``` will return a similar parsable output :

----
192.168.13.53 | SUCCESS => {
    "ansible_facts": {
        "ansible_default_ipv4": {
            "address": "192.168.13.53",
            "alias": "ens3",
            "broadcast": "192.168.13.255",
            "gateway": "192.168.13.1",
            "interface": "ens3",
            "macaddress": "00:50:56:7a:3e:d9",
            "mtu": 1500,
            "netmask": "255.255.255.0",
            "network": "192.168.13.0",
            "type": "ether"
        }
    },
    "changed": false
}
----

==== ru_group group_name N...

Calling ru_group will rearange inventory by creating a new inventory group containing servers which indexes are past as argument.

This function comes with two others:

- ru_groups will list all existing groups in inventory
- ru_group_servers will list all servers in a group passed as argument

==== ru_server and ru_servers

Those functions return either a single server name from index, or the list.

==== ru_server_nums

Returns the list of indexes to address the server through functions like ru_server or ru_run.

==== ru_uuid

Returns an identifier, using what is avilable on the host (uuidgen, uuid...) and if no other option is available, fallbacks to using the date.

=== debug mode

In the standard usage, role_unit executes the following sequence:

- containers creation
- application of automation to the container(s)
- execution of tests through bash_unit
- containers destruction

if you set RU_DEBUG environment variable to 1, the sequence will be:

- search for an existing debug environment
- containers creation if none found
- application of automation to the container(s)
- execution of tests through bash_unit

containers are then not removed and instructions are given to enter the container to do actual debugging and dispose of the environment when debugging is done.

if tests are run without debug, any existing debug environment matching test file and backend will be destroyed.

== advanced usage

=== documentation through examples

This part of the documentation is created in an effort to provide usage examples.

Available documentations for now:

- link:playbook_rewrite/[playbook rewrite] - how to rewrite testing playbook in order to test complex scenarios
